"""
Vector Module

References:
    Unity3D's Vector3 class:
        https://docs.unity3d.com/ScriptReference/Vector3.html
        https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Math/Vector2.cs
        https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Math/Vector3.cs
        https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Math/Vector4.cs
        https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Math/Quaternion.cs

    https://stackoverflow.com/questions/43541934/implementing-3d-vectors-in-python-numpy-vs-x-y-z-fields
"""
# ----------------------------------------------------------------------------------------------------------------------
# region Modules
#
import math
from datetime import datetime
import random
#import numpy # might not be always available, so operate under that assumption
#endregion Modules

number_types = (int, float)

# ----------------------------------------------------------------------------------------------------------------------
# region Classes
#
class Vector():
    """
    Vector class for 3D fun.

    Arguments:
        Vector(x [, y, z, w]) - Create new Vector instance with 2 to 4 properties (Vector2, Vector3, Vector4)
        Vector("x , y, z, w") - Create new Vector instance by parsing the contents of a string
        Vector([x, y, z, w]) - Create new Vector instance from a list of up to 4 numbers.
        Vector(Vector) - Create a new Vector instance, duplicating the values from the source Vector

    Returns:
        Vector -- The vector class
    """

    # ------------------------------------------------------------------------------
    #region __init__
    #
    def __init__(self, *args):
        self.x = 0.0
        self.y = 0.0
        self.z = None
        self.w = None

        #TODO: Look into storing this as a list instead of as individual properties, look into the @property system
        self.data = [0.0, 0.0]

        if len(args) == 1:
            if isinstance(args[0], str) and "," in args[0]: # Vector("#,#,#")
                args = args[0].split(",")

            elif isinstance(args[0], list): # Vector([#,#,#])
                args = args[0]

            elif isinstance(args[0], Vector): # Vector(source_vector)
                args = [args[0][i] for i in range(len(args[0]))] # create a new args list with the source vector's properties

        # Assign and convert to float
        for i in range(len(args)):
            self[i] = float(args[i]) if not isinstance(args[i], float) else args[i]
    #endregion __init__

    # ------------------------------------------------------------------------------
    #region Properties
    #
    @property
    def x1(self):
        return self.data[0]
    @x1.setter
    def x1(self, value):
        self.data[0] = float(value)
    @x1.deleter
    def x1(self):
        self.data[0] = 0.0

    @property
    def y1(self):
        return self.data[1]
    @y1.setter
    def y1(self, value):
        self.data[1] = float(value)
    @y1.deleter
    def y1(self):
        self.data[1] = 0.0

    @property
    def z1(self):
        if len(self.data) <= 2: # only x, y
            self.data.append(0.0) # add .z
        return self.data[2]
    @z1.setter
    def z1(self, value):
        if len(self.data) <= 2:
            self.data.append(0.0)
        self.data[2] = float(value)
    @z1.deleter
    def z1(self):
        if len(self.data) >= 2:
            if len(self.data) >= 3: # delete .w first
                del self.data[3]
            del self.data[2] # .z

    @property
    def w1(self):
        if len(self.data) <= 3:
            if len(self.data) <= 2: # add .z
                self.data.append(0.0)
            self.data.append(0.0) # add .w
        return self.data[3]
    @w1.setter
    def w1(self, value):
        if len(self.data) <= 3:
            if len(self.data) <= 2: # add .z
                self.data.append(0.0)
            self.data.append(0.0) # add .w
        self.data[3] = float(value)
    @w1.deleter
    def w1(self):
        if len(self.data) >= 3:
            del self.data[3] # .w
    #endregion

    # ------------------------------------------------------------------------------
    #region Magic Methods - https://www.python-course.eu/python3_magic_methods.php
    #
    def __repr__(self):
        """Returns this class instance's data."""
        return "Vector({})".format(",".join([str(self[i]) for i in range(len(self))]))

    def __str__(self):
        """Returns a readable form of this class instance's data."""
        return "<{}>".format(", ".join([str(self[i]) for i in range(len(self))]))

    def __getitem__(self, index):
        """
        Get the value of index of this Vector instance

        Arguments:
            index {int} -- The index property to retrieve (0 = x, 1 = y, etc.)

        Returns:
            float -- The value of that property index.
        """
        if index == 0:
            return self.x
        elif index == 1:
            return self.y
        elif index == 2:
            return self.z
        elif index == 3:
            return self.w
        else:
            raise IndexError

    def __setitem__(self, index, value):
        """
        Set the value of index of this Vector instance

        Arguments:
            index {[type]} -- [description]
            value {[type]} -- [description]
        """
        if index == 0:
            self.x = value
        elif index == 1:
            self.y = value
        elif index == 2:
            self.z = value
        elif index == 3:
            self.w = value
        else:
            raise IndexError

    def __len__(self):
        #return len(self.data)
        if self.w is not None:
            return 4
        if self.z is not None:
            return 3
        return 2
    #endregion Magic Methods

    # ------------------------------------------------------------------------------
    #region Binary Operators
    #
    def __add__(self, other): # self + other
        if isinstance(other, Vector) and len(self) == len(other): # Vector + Vector
            return Vector([self[i]+other[i] for i in range(len(self))])
        if isinstance(other, number_types): # Vector + int/float
            return Vector([self[i]+other for i in range(len(self))])
        raise ValueError("Other argument is of an invalid type ({})".format(type(other).__name__))

    def __sub__(self, other): # self - other
        if isinstance(other, Vector) and len(self) == len(other): # Vector - Vector
            return Vector([self[i]-other[i] for i in range(len(self))])
        if isinstance(other, number_types): # Vector - int/float
            return Vector([self[i]-other for i in range(len(self))])
        raise ValueError("Other argument is of an invalid type ({})".format(type(other).__name__))

    def __mul__(self, other): # self * other
        if isinstance(other, Vector) and len(self) == len(other): # Vector * Vector
            return Vector([self[i]*other[i] for i in range(len(self))])
        if isinstance(other, number_types): # Vector * int/float
            return Vector([self[i]*other for i in range(len(self))])
        raise ValueError("Other argument is of an invalid type ({})".format(type(other).__name__))

    def __div__(self, other): # self * other
        if isinstance(other, Vector) and len(self) == len(other): # Vector * Vector
            return Vector([self[i]/other[i] for i in range(len(self))])
        if isinstance(other, number_types): # Vector * int/float
            return Vector([self[i]/other for i in range(len(self))])
        raise ValueError("Other argument is of an invalid type ({})".format(type(other).__name__))

    def __truediv__(self, other): # self / other
        if isinstance(other, Vector) and len(self) == len(other): # Vector / Vector
            return Vector([self[i]/other[i] for i in range(len(self))])
        if isinstance(other, number_types): # Vector / int/float
            return Vector([self[i]/other for i in range(len(self))])
        raise ValueError("Other argument is of an invalid type ({})".format(type(other).__name__))

    def __floordiv__(self, other): # self // other
        if isinstance(other, Vector) and len(self) == len(other): # Vector // Vector
            return Vector([self[i]//other[i] for i in range(len(self))])
        if isinstance(other, number_types): # Vector // int/float
            return Vector([self[i]//other for i in range(len(self))])
        raise ValueError("Other argument is of an invalid type ({})".format(type(other).__name__))

    def __mod__(self, other): # self % other
        if isinstance(other, Vector) and len(self) == len(other): # Vector % Vector
            return Vector([self[i]%other[i] for i in range(len(self))])
        if isinstance(other, number_types): # Vector % int/float
            return Vector([self[i]%other for i in range(len(self))])
        raise ValueError("Other argument is of an invalid type ({})".format(type(other).__name__))

    def __pow__(self, other): # self ** other
        if isinstance(other, Vector) and len(self) == len(other): # Vector ** Vector
            return Vector([self[i]**other[i] for i in range(len(self))])
        if isinstance(other, number_types): # Vector ** int/float
            return Vector([self[i]**other for i in range(len(self))])
        raise ValueError("Other argument is of an invalid type ({})".format(type(other).__name__))
    #endregion Binary Operators

    # ------------------------------------------------------------------------------
    #region Extended Assignments
    #
    # += 	object.__iadd__(self, other)
    # -= 	object.__isub__(self, other)
    # *= 	object.__imul__(self, other)
    # /= 	object.__idiv__(self, other)
    # //= 	object.__ifloordiv__(self, other)
    # %= 	object.__imod__(self, other)
    # **= 	object.__ipow__(self, other[, modulo])
    #endregion Extended Assignments

    # ------------------------------------------------------------------------------
    #region Operators - Unary
    #
    def __neg__(self): # -x
        return Vector([-self[i] for i in range(len(self))])

    def __abs__(self): # abs(x)
        return Vector([abs(self[i]) for i in range(len(self))])

    # + 	object.__pos__(self)
    # ~ 	object.__invert__(self)
    # complex() 	object.__complex__(self)
    # int() 	object.__int__(self)
    # long() 	object.__long__(self)
    # float() 	object.__float__(self)
    # oct() 	object.__oct__(self)
    # hex() 	object.__hex__(self
    #endregion Operators - Unary

    # ------------------------------------------------------------------------------
    #region Operators - Comparisons
    #
    def __lt__(self, other): # self < other
        if isinstance(other, Vector) and len(self) == len(other): # Vector < Vector
            if False in [(self[i] < other[i]) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        if isinstance(other, number_types): # Vector < int/float
            if False in [(self[i] < other) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        # other is not a valid type, or mismatched lengths
        raise ValueError

    def __le__(self, other): # self <= other
        if isinstance(other, Vector) and len(self) == len(other): # Vector <= Vector
            if False in [(self[i] <= other[i]) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        if isinstance(other, number_types): # Vector <= int/float
            if False in [(self[i] <= other) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        # other is not a valid type, or mismatched lengths
        raise ValueError

    def __eq__(self, other): # self == other
        if isinstance(other, Vector) and len(self) == len(other): # Vector == Vector
            if False in [(self[i] == other[i]) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        if isinstance(other, number_types): # Vector == int/float
            if False in [(self[i] == other) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        # other is not a valid type, or mismatched lengths
        raise ValueError

    def __ne__(self, other): # self != other
        if isinstance(other, Vector) and len(self) == len(other): # Vector != Vector
            if False in [(self[i] != other[i]) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        if isinstance(other, number_types): # Vector != int/float
            if False in [(self[i] != other) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        # other is not a valid type, or mismatched lengths
        raise ValueError

    def __gt__(self, other): # self > other
        if isinstance(other, Vector) and len(self) == len(other): # Vector > Vector
            if False in [(self[i] > other[i]) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        if isinstance(other, number_types): # Vector > int/float
            if False in [(self[i] > other) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        # other is not a valid type, or mismatched lengths
        raise ValueError

    def __ge__(self, other): # self >= other
        if isinstance(other, Vector) and len(self) == len(other): # Vector >= Vector
            if False in [(self[i] >= other[i]) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        if isinstance(other, number_types): # Vector >= int/float
            if False in [(self[i] >= other) for i in range(len(self))]: # Check if one or more comparisons failed
                return False
            return True
        # other is not a valid type, or mismatched lengths
        raise ValueError
    #endregion Operators - Comparisons

    # ------------------------------------------------------------------------------
    #region Methods
    #
    def magnitude(self):
        """
        Returns the magnitude of the vector.

        Returns:
            float -- [description]
        """
        return math.sqrt(sum(self[i]*self[i] for i in range(len(self))))

    def sqrmagnitude(self):
        """
        Returns the squared magnitude of the vector. Useful to skip the expensive sqrt() function if speed is required.

        Returns:
            float -- [description]
        """
        return sum(self[i]*self[i] for i in range(len(self)))

    def normalized(self):
        """
        Returns the normalized vector.

        Returns:
            Vector -- [description]
        """
        m = self.magnitude()
        return Vector([self[i]/m for i in range(len(self))])

    def normalize(self):
        """
        Normalizes the values of this Vector instance.
        """
        v = self.normalized()
        for i in range(len(self)):
            self[i] = v[i]

    def round(self, *argv):
        """
        Returns the vector rounds to x decimal places.

        Args:
            ndigits ([type], optional): [description]. Defaults to None.

        Returns:
            [type]: [description]
        """

        v = Vector(self)

        if argv and len(argv) == 1:
            ndigits = int(argv[0]) # decimal places
            if v.x:
                v.x = round(v.x, ndigits)
            if v.y:
                v.y = round(v.y, ndigits)
            if v.z:
                v.z = round(v.z, ndigits)
            if v.w:
                v.w = round(v.w, ndigits)
        else:
            if v.x:
                v.x = round(v.x)
            if v.y:
                v.y = round(v.y)
            if v.z:
                v.z = round(v.z)
            if v.w:
                v.w = round(v.w)
        return v

    def rounded(self, *argv):
        """
        Rounds the values of this Vector instance.
        """
        if argv and len(argv) == 1:
            ndigits = int(argv[0]) # decimal places
            if self.x:
                self.x = round(self.x, ndigits)
            if self.y:
                self.y = round(self.y, ndigits)
            if self.z:
                self.z = round(self.z, ndigits)
            if self.w:
                self.w = round(self.w, ndigits)
        else:
            if self.x:
                self.x = round(self.x)
            if self.y:
                self.y = round(self.y)
            if self.z:
                self.z = round(self.z)
            if self.w:
                self.w = round(self.w)


    def to_list(self, sort=False, reverse=False):
        """
        Returns the Vector as a list.

        Args:
            sort (bool, optional): If true, will sort the list in ascending order. Defaults to False.
            reverse (bool, optional): If true, will reverse the list order (happens after the sort if enabled). Defaults to False.

        Returns:
            list: The Vector as a list of floats.
        """
        l = [self.x, self.y, self.z, self.w]

        if sort:
            l.sort()
        if reverse:
            l.reverse()

        return l
    #endregion


    # ------------------------------------------------------------------------------
    #region Class Methods - Shorthand Vector stuff
    #
    @classmethod
    def zero(cls, size=3):
        """
        Returns a Vector of size elements, with each value being 0.

        Keyword Arguments:
            size {int} -- Size of the vector (2, 3, or 4) (default: {3})

        Returns:
            Vector -- Vector(0, 0, 0, 0)
        """
        return Vector([0 for i in range(size)])

    @classmethod
    def one(cls, size=3):
        """
        Returns a Vector of [size] elements, with each value being 1.

        Keyword Arguments:
            size {int} -- [description] (default: {3})

        Returns:
            Vector -- Vector(1, 1, 1, 1)
        """
        return Vector([1 for i in range(size)])

    @classmethod
    def right(cls, size=3):
        """Shorthand for Vector(1, 0, 0)"""
        v = Vector.zero(size=size)
        v.x = 1.0
        return v

    @classmethod
    def left(cls, size=3):
        """Shorthand for Vector(-1, 0, 0)"""
        v = Vector.zero(size=size)
        v.x = -1.0
        return v

    @classmethod
    def up(cls, size=3):
        """Shorthand for Vector(0, 1) or Vector(0, 0, 1)"""
        v = Vector.zero(size=size)
        if size == 2:
            v.y = 1.0
        if size == 3:
            v.z = 1.0
        return v

    @classmethod
    def down(cls, size=3):
        """Shorthand for Vector(0, 1) or Vector(0, 0, -1)"""
        v = Vector.zero(size=size)
        if size == 2:
            v.y = -1.0
        if size == 3:
            v.z = -1.0
        return v

    @classmethod
    def forward(cls):
        """Shorthand for Vector(0, 1, 0)"""
        return Vector(0, 1.0, 0)

    @classmethod
    def backward(cls):
        """Shorthand for Vector(0, -1, 0)"""
        return Vector(0, -1.0, 0)

    @classmethod
    def none(cls):
        """Shorthand for Vector(None, None, None), might be useful for specific cases."""
        v = Vector(0, 0, 0)
        v.x = None
        v.y = None
        v.z = None
        return v
    #endregion


    # ------------------------------------------------------------------------------
    #region Class Methods - General Math Functions
    #
    @classmethod
    def Cross(cls, lhs, rhs):
        """
        Cross Product of two vectors.

        Arguments:
            lhs {Vector} -- [description]
            rhs {Vector} -- [description]

        Returns:
            Vector -- [description]
        """
        if not isinstance(lhs, Vector) or not isinstance(rhs, Vector):
            raise ValueError("Arguments are of an invalid type")
        if len(lhs) != len(rhs):
            return IndexError
        if len(lhs) != 3: # How to Handle Vector2 and Vector4?
            return IndexError

        return Vector(
            lhs.y * rhs.z - lhs.z * rhs.y,
            lhs.z * rhs.x - lhs.x * rhs.z,
            lhs.x * rhs.y - lhs.y * rhs.x
        )

    @classmethod
    def Dot(cls, lhs, rhs):
        """
        Dot product of two vectors.

        Arguments:
            lhs {Vector} -- [description]
            rhs {Vector} -- [description]

        Returns:
            float -- positive if in the same direction. 0 if perpendicular, negative if mirrored
        """
        if not isinstance(lhs, Vector) or not isinstance(rhs, Vector):
            raise ValueError
        if len(lhs) != len(rhs):
            return ValueError

        # if lhs.w is not None: # Vector4
        #     return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z + lhs.w * rhs.w
        # if lhs.z is not None: # Vector3
        #     return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z
        # return lhs.x * rhs.x + lhs.y * rhs.y # Vector2
        r = [lhs[i] * rhs[i] for i in range(len(lhs))]
        return sum(r)

    @classmethod
    def Distance(cls, a, b):
        """
        Returns the distance between a and b

        Arguments:
            a {Vector} -- [description]
            b {Vector} -- [description]

        Raises:
            ValueError: If there is a mismatch in the size of the Vectors

        Returns:
            float -- Distance between the two.
        """
        if not isinstance(a, Vector) or not isinstance(b, Vector):
            raise ValueError
        if len(a) != len(b):
            return ValueError

        # v = Vector([a[i] - b[i] for i in range(len(a))]) # get the direction from a to b
        v = a - b # get the direction from a to b
        v = Vector.Dot(v, v) # square each element, then the dot will add them up: dot = sum(x**x, y**y, z**z, w**w)
        return math.sqrt(v)

    @classmethod
    def Min(cls, *args):
        """
        Returns a vector where it's components are the minimum of the cooresponding source vectors' components.
        i.e. Given vectors with the values of (1, 2, 3), (3, 2, 1), (1, 1, 0) - it will return (1, 2, 0)

        Arguments:
            *args {Vector} -- [description]

        Raises:
            ValueError: [description]

        Returns:
            Vector -- A Vector where the
        """
        if len(args) == 1 and isinstance(args[0], list): # passed a list of vectors, convert to the full args
            args = args[0]

        vector_length = []
        for v in args:
            if not isinstance(v, Vector): # is not a Vector class
                raise ValueError("{} is not a Vector class.".format(v))
            if len(v) not in vector_length: # gather lengths
                vector_length.append(len(v))

        if len(vector_length) != 1:
            raise ValueError("Multiple vector lengths. They should all be the same size.")

        min_vector = Vector.zero()
        for i in range(vector_length[0]): # loop each vector component
            # gather the values from each vector into a list and find the min
            min_vector[i] = min([args[j][i] for j in range(len(args))])

        return min_vector

    @classmethod
    def Max(cls, *args):
        """
        Returns a vector where it's components are the maximum of the source vectors.
        i.e. a(1, 2, 3) and b(3, 2, 1) will return c(3, 2, 3)

        Arguments:
            *args {Vector} -- [description]

        Returns:
            Vector -- [description]
        """
        if len(args) == 1 and isinstance(args[0], list): # passed a list of vectors, convert to the full args
            args = args[0]

        vector_length = []
        for v in args:
            if not isinstance(v, Vector): # is not a Vector class
                raise ValueError("{} is not a Vector class.".format(v))
            if len(v) not in vector_length: # gather lengths
                vector_length.append(len(v))

        if len(vector_length) != 1:
            raise ValueError("Multiple vector lengths. They should all be the same size.")

        max_vector = Vector.zero()
        for i in range(vector_length[0]): # loop each vector component
            # gather the values from each vector into a list and find the max
            max_vector[i] = max([args[j][i] for j in range(len(args))])

        return max_vector

    @classmethod
    def Clamp(cls, v, min_value, max_value):
        """
        Clamps the value or each component of a vector to be within the range of min_value to max_value.

        Arguments:
            v {Vector, int, float} -- The vector, int, or float to clamp its values
            min_value {float} -- The min value allowed
            max_value {float} -- The max value allowed

        Raises:
            ValueError -- if the source value is not a Vector, int, or float.

        Returns:
            int/float -- The value clamped to min_value and max_value
            Vector -- A vector with each component clamped to min_value and max_value
        """
        if isinstance(v, Vector):
            return [Vector.Clamp(v[i], min_value, max_value) for i in range(len(v))]
        elif isinstance(v, number_types):
            return max(min(v, max_value), min_value)
        raise ValueError
    #endregion


    # ------------------------------------------------------------------------------
    #region Class Methods - Animation and Rotation
    #
    @classmethod
    def Lerp(cls, a, b, t):
        """
        Linearly interpolates between two vectors.

        Arguments:
            a {float} -- Vector start
            b {float} -- Vector end
            t {float} -- Time between 0 and 1.

        Returns:
            [type] -- [description]
        """
        if isinstance(a, Vector) and isinstance(b, Vector) and len(a) == len(b) and isinstance(t, (int, float)):
            t = max(min(t, 1.0), 0.0) # Clamp t to 0.0-1.0
            return Vector([(a[i] + (b.x - a.x) * t) for i in range(len(a))])
        raise ValueError("Other argument is of an invalid type ({})".format(type(b).__name__))

    @classmethod
    def Angle(cls, f, t, axis=None, precision=6):
        """
        Returns the (smaller) angle of the two vectors, between -180 to 180 degrees.

        Arguments:
            f {Vector} -- From vector
            t {Vector} -- To vector

        Keyword Arguments:
            axis {Vector} -- If supplied, uses axis to calculate the signed angle. Ideally it would be perpendicular to the two vectors, i.e. if f and t is on the X/Y axis, axis would be <0,0,1> Vector.up (default: {None})
            precision {int} -- Clamp to x decimal places. Set to None to have no rounding of the angle value. (default: 6)

        Returns:
            float -- The angle in degrees. If axis is supplied, then it will be a signed angle (+angle for clockwise, -angle for counter-clockwise, from axis's +Z view)
        """
        if not isinstance(f, Vector) and not isinstance(t, Vector):
            return ValueError
        if len(f) != len(t):
            return ValueError

        denominator = math.sqrt(f.sqrmagnitude() * t.sqrmagnitude())
        # check for min value allowed
        print("denominator = {}".format(denominator))

        dot = Vector.Dot(f, t) / denominator
        print("Vector.Dot(f, t) / denominator = {}".format(dot))
        dot = Vector.Clamp(dot, -1.0, 1.0) # Clamp -1.0 to +1.0
        print("Vector.Dot(f, t) / denominator = {}".format(dot))
        a = math.degrees(math.acos(dot))
        print("math.degrees(math.acos(dot)) = {}".format(a))

        if isinstance(axis, Vector):
            c = Vector.Cross(f, t)
            d2 = Vector.Dot(axis.normalized(), c.normalized())
            sign = 1 if d2 >= 0 else -1
            a = a * sign

        # Clamp the x decimal places, since some values can return with precision like 45.00000000000001 (14 decimal places)
        if precision:
            a = float("{:.{precision}f}".format(a, precision=int(precision)))

        return a

    @classmethod
    def RotatePoint(cls, rotation, point):
        """
        Rotates a point/vector around the origin (Vector(0,0,0))

        Arguments:
            rotation {Vector3} -- In degrees, +Y is the object forward, x = pitch, y = roll, z = yaw
            point {Vector3} -- The point to rotate around.

        Returns:
            Vector3 -- The new rotated point
        """
        if not isinstance(rotation, Vector) or not isinstance(point, Vector):
            raise ValueError
        if not (len(rotation) == 3 or len(rotation) == 4) or len(point) != 3:
            raise ValueError

        # Rotation = Euler, convert to Quaternion
        if len(rotation) == 3:
            # Convert from degrees to radians, then assign to the rotation axis
            # https://math.stackexchange.com/questions/2975109/how-to-convert-euler-angles-to-quaternions-and-get-the-same-euler-angles-back-fr
            # note: example above uses X = roll, so we swap - need to rework the math to fix the mismatch
            # If +Y is forward, x = pitch, y = roll, z = yaw
            roll = math.radians(rotation.x) # should be pitch
            pitch = math.radians(rotation.y) # should be roll
            yaw = math.radians(rotation.z) # yaw

            # Convert from Euler to Quaternion
            qx = math.sin(roll/2) * math.cos(pitch/2) * math.cos(yaw/2) - math.cos(roll/2) * math.sin(pitch/2) * math.sin(yaw/2)
            qy = math.cos(roll/2) * math.sin(pitch/2) * math.cos(yaw/2) + math.sin(roll/2) * math.cos(pitch/2) * math.sin(yaw/2)
            qz = math.cos(roll/2) * math.cos(pitch/2) * math.sin(yaw/2) - math.sin(roll/2) * math.sin(pitch/2) * math.cos(yaw/2)
            qw = math.cos(roll/2) * math.cos(pitch/2) * math.cos(yaw/2) + math.sin(roll/2) * math.sin(pitch/2) * math.sin(yaw/2)
            rotation = Vector(qx, qy, qz, qw)
        # elif len(rotation) == 4: # assume this is passing in a quat?

        x = rotation.x * 2.0
        y = rotation.y * 2.0
        z = rotation.z * 2.0
        xx = rotation.x * x
        yy = rotation.y * y
        zz = rotation.z * z
        xy = rotation.x * y
        xz = rotation.x * z
        yz = rotation.y * z
        wx = rotation.w * x
        wy = rotation.w * y
        wz = rotation.w * z

        p = Vector(0, 0, 0)
        p.x = (1.0 - (yy + zz)) * point.x + (xy - wz) * point.y + (xz + wy) * point.z
        p.y = (xy + wz) * point.x + (1.0 - (xx + zz)) * point.y + (yz - wx) * point.z
        p.z = (xz - wy) * point.x + (yz + wx) * point.y + (1.0 - (xx + yy)) * point.z
        return p
    #endregion


    # ------------------------------------------------------------------------------
    #region Class Methods - Geometry
    #
    @classmethod
    def AddVectorLength(cls, vector, size):
        """
        Increase/Decrease the vector length by 'size' units.

        Arguments:
            vector {Vector} -- The vector to modify
            size {float} -- Modify the vector by 'size'
        """
        # Get the vector length
        magnitude = vector.magnitude()

        # Modify its size
        magnitude += size

        # Normalize the vector
        vector_normalized = vector.normalized()

        # Scale the vector
        return vector_normalized * magnitude

    @classmethod
    def SetVectorLength(cls, vector, size):
        """
        Create a vector of direction "vector" with length "size"

        Arguments:
            vector {Vector} -- The vector direction
            size {float} -- Size of the vector
        """
        # Normalize the vector
        vector_normalized = vector.normalized()

        # Scale the vector
        return vector_normalized * size

    @classmethod
    def LineLineIntersection(cls, linePoint1, lineVector1, linePoint2, lineVector2):
        """
        Calculate the intersection point of two lines. Returns true if lines intersect, otherwise false.
	    Note that in 3d, two lines do not intersect most of the time. So if the two lines are not in the
	    same plane, use ClosestPointsOnTwoLines() instead.

        Arguments:
            linePoint1 {[type]} -- [description]
            lineVector1 {[type]} -- [description]
            linePoint2 {[type]} -- [description]
            lineVector2 {[type]} -- [description]

        Returns:
            Vector -- Of the intersection point, or False if the lines do not intersect.
        """

        intersection = Vector.zero()

        lineVec3 = linePoint2 - linePoint1
        crossVec1and2 = Vector.Cross(lineVector1, lineVector2)
        crossVec3and2 = Vector.Cross(lineVec3, lineVector2)
        planarFactor = Vector.Dot(lineVec3, crossVec1and2)

        # Lines are not coplanar. Take into account rounding errors.
        if ((planarFactor >= 0.00001) or (planarFactor <= -0.00001)):
            return False

        # Note: sqrMagnitude does x*x+y*y+z*z on the input vector.
        s = Vector.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrmagnitude()
        if s >= 0.0 and s <= 1.0:
            intersection = linePoint1 + (lineVector1 * s)
            return intersection
        else:
            return False

    @classmethod
    def ClosestPointsOnTwoLines(cls, line1point, line1vector, line2point, line2vector):
        """
        http://wiki.unity3d.com/index.php/3d_Math_functions

        Two non-parallel lines which may or may not touch each other have a point on each line which are closest
        to each other. This function finds those two points. If the lines are not parallel, the function
        outputs true, otherwise false.

        Arguments:
            line1point1 {Vector} -- Line 1's starting point
            line1vector {Vector} -- Line 1's vector (direction, distance)
            line2point1 {Vector} -- Line 2's starting point
            line2vector {Vector} -- Line 2's vector (direction, distance)


        Returns:
            [type] -- [description]
        """

        line1closestpoint = Vector.zero()
        line2closestpoint = Vector.zero()

        a = Vector.Dot(line1vector, line1vector)
        b = Vector.Dot(line1vector, line2vector)
        e = Vector.Dot(line2vector, line2vector)

        d = a * e - b * b

        if d != 0: # Not parallel
            r = line1point - line2point
            c = Vector.Dot(line1vector, r)
            f = Vector.Dot(line2vector, r)

            s = (b * f - c * e) / d
            t = (a * f - c * b) / d

            line1closestpoint = line1point + line1vector * s
            line2closestpoint = line2point + line2vector * t
            return [line1closestpoint, line2closestpoint]
        else:
            return False

    #endregion Class Methods
#endregion Classes


# ----------------------------------------------------------------------------------------------------------------------
# region Main
#
def main():

    line1point = Vector(-1, 0, 0)
    line1vector = Vector(2, 0, 0)
    line2point = Vector(0, -1, 0)
    line2vector = Vector(0, 2, 0)
    points = Vector.LineLineIntersection(line1point, line1vector, line2point, line2vector)
    print("LineLineIntersection: {}".format(points))
    points = Vector.ClosestPointsOnTwoLines(line1point, line1vector, line2point, line2vector)
    print("ClosestPointsOnTwoLines: {}".format(points))



if __name__ in ["__main__", "<module>"]:
    main()
    print("\n# ----------------------------------------------------------------------------------------------------------------------\n# Done\n#\n")
#endregion Main
