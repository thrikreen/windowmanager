"""lib/dcc"""

# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import lib.settings as settings
from lib.Log import Log

# DCC specific modules
Log("lib/dcc: {}".format(settings.DCCPLATFORM))

if settings.DCCPLATFORM in [settings.STANDALONE]:
    from lib.dcc.dummy import common
    from lib.dcc.dummy import geometry

elif settings.DCCPLATFORM in [settings.MAX]:
    from lib.dcc.max import common
    from lib.dcc.max import geometry
# elif settings.DCCPLATFORM == settings.MAYA:
#     print("Loading Maya")
# elif settings.DCCPLATFORM == settings.BLENDER:
#     print("Loading Blender")

#endregion Modules


# ----------------------------------------------------------------------------------------------------------------------
#region
#

#endregion


# ----------------------------------------------------------------------------------------------------------------------
#region Common Functions
#

#endregion Common Functions
