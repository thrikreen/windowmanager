from lib.Log import Log

# ----------------------------------------------------------------------------------------------------------------------
#region Object Selection
#
def GetNodeByName(node, verbose=True):
    """
    GetNodeByName [summary]

    Args:
        verbose (bool, optional): [description]. Defaults to True.
    """
    output = []
    if verbose:
        Log(output, node)

def GetSelection(filter=None, verbose=True):
    """
    GetSelection [summary]

    Args:
        verbose (bool, optional): [description]. Defaults to True.
    """
    output = []
    objects = []

    if verbose:
        Log(output)

    return objects

#endregion Object Selection
