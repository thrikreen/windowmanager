from lib.Log import Log

import MaxPlus #pylint: disable=import-error
import pymxs #pylint: disable=import-error
import lib.dcc.max.geometry
import lib.dcc.max.geometry.vertex
import lib.dcc.max.geometry.edge
import lib.dcc.max.geometry.face
import lib.dcc.max.geometry.uv


# ----------------------------------------------------------------------------------------------------------------------
#region - Class
#
class DCCNode(object):

    # ------------------------------------------------------------------------------------------------------------------
    #region - Init
    #
    def __init__(self, node):
        self.name = None

        self.node = node
        self.handle = None

    #endregion - Init

    # ------------------------------------------------------------------------------------------------------------------
    #region -
    #

    #endregion -

    # ------------------------------------------------------------------------------------------------------------------
    #region - Transforms
    #
    def GetPosition(self):
        pass

    def SetPosition(self, newposition):
        pass

    def GetRotation(self):
        pass

    def SetRotation(self, newrotation):
        pass

    def GetScale(self):
        pass

    def SetScale(self, newscale):
        pass
    #endregion - Transforms

    # ------------------------------------------------------------------------------------------------------------------
    #region - Bounds
    #
    def GetBounds(self):
        pass

    def GetBoundsMin(self):
        pass

    def GetBoundsMax(self):
        pass

    def GetBoundsSize(self):
        pass

    def GetBoundsCenter(self):
        pass
    #endregion - Bounds

    # ------------------------------------------------------------------------------------------------------------------
    #region - Vertex
    #
    def GetVertexCount(self):
        pass

    def GetVertex(self, index):
        pass

    def SetVertex(self, index, position):
        pass
    #endregion - Vertex

    # ------------------------------------------------------------------------------------------------------------------
    #region - Edge
    #
    def GetEdgeCount(self):
        pass

    def GetEdge(self, index):
        pass

    def SetEdge(self, index, vertices):
        pass
    #endregion - Edge

    # ------------------------------------------------------------------------------------------------------------------
    #region - Face
    #
    def GetFaceCount(self):
        pass

    def GetFace(self, index):
        pass

    def SetFace(self, index, vertices):
        pass
    #endregion - Face

    # ------------------------------------------------------------------------------------------------------------------
    #region - UV
    #
    def GetUVCount(self):
        pass

    def GetUV(self, index):
        pass

    def SetUV(self, index, vertices, materialid):
        pass
    #endregion - UV




#endregion - Class