"""lib/dcc/max/geometry/vertex"""
from lib.Log import Log

# ----------------------------------------------------------------------------------------------------------------------
#region Functions
#
def GetVertexCount(node, verbose=True):
    """
    GetVertexCount [summary]

    Args:
        node ([type]): [description]
        verbose (bool, optional): [description]. Defaults to True.

    Returns:
        [type]: [description]
    """
    output = []
    element_count = 0

    output += ["{} ({})".format(node, node.GetHandle())]




    if verbose:
        Log(output, node)

    return element_count

#endregion Functions
