from lib.Log import Log
import MaxPlus
import pymxs

# ----------------------------------------------------------------------------------------------------------------------
#region Object Selection
#
def GetNodeByName(node, verbose=True):
    """
    GetNodeByName [summary]

    Args:
        verbose (bool, optional): [description]. Defaults to True.
    """
    output = []
    if verbose:
        Log(output, node)

def GetSelection(filter=None, verbose=True):
    """
    GetSelection [summary]

    Args:
        verbose (bool, optional): [description]. Defaults to True.
    """
    output = []
    objects = []

    selected_objects = MaxPlus.SelectionManager.Nodes
    for obj in selected_objects:
        if filter: # Filter by object type
            pass
        else:
            objects.append(obj)

    if verbose:
        Log(output)

    return objects

#endregion Object Selection
