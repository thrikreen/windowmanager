"""
WindowManager unit tests
"""
# --------------------------------------------------------------------------------------------------
#region Modules
#
from lib.ToolDockWidget import ToolDockWidget
import os
import sys
import random
import unittest

import Qt # https://github.com/mottosso/Qt.py
from Qt import QtGui, QtWidgets, QtCore, QtCompat, QtTest #pylint: disable=no-name-in-module

import lib.settings as settings
from toolmanager import ToolManager
#endregion Modules

# --------------------------------------------------------------------------------------------------
#region Globals
#
# Need to run this to init Qt
QAPP = QtWidgets.QApplication(sys.argv)

APP_ROOT = settings.sanitize_path(os.path.dirname(os.path.dirname(__file__)).replace("c:", "C:"))
if APP_ROOT not in sys.path:
    sys.path.append(APP_ROOT)
FILEPATH = settings.sanitize_path(os.path.join(APP_ROOT, "launch_toolmanager.py"))
SETTINGSPATH = settings.sanitize_path(os.path.join(os.path.expanduser("~"), "toolmanager/toolmanager.ini"))
SETTINGSPATH2 = settings.sanitize_path(os.path.join(APP_ROOT, "unittests/toolmanager.ini")) # use this file for testing against
MODULEPATH = settings.sanitize_path(os.path.join(APP_ROOT, "modules"))
MODULEPATH2 = settings.sanitize_path(os.path.join(os.path.expanduser("~"), "toolmanager/modules"))

print("QAPP: {}".format(QAPP))
print("APP_ROOT: {}".format(APP_ROOT))
print("FILEPATH: {}".format(FILEPATH))
print("SETTINGSPATH: {}".format(SETTINGSPATH))
print("SETTINGSPATH2: {}".format(SETTINGSPATH2))
print("MODULEPATH: {}".format(MODULEPATH))
print("MODULEPATH2: {}".format(MODULEPATH2))
#endregion Globals

ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

# --------------------------------------------------------------------------------------------------
#region Tests
#
class Test_FileHandling(unittest.TestCase):
    """Test the file handling functions in the settings module."""
    #pylint: disable=missing-function-docstring

    def test_is_dir(self):
        result = None
        try:
            result = settings.dir_path(APP_ROOT)
        except NotADirectoryError as e:
            print("[ERROR] test_is_dir: {}".format(e))
        self.assertEqual(APP_ROOT, result)

    def test_is_not_dir(self):
        result = None
        try:
            result = settings.dir_path(FILEPATH)
        except NotADirectoryError as e:
            print("[ERROR] test_is_not_dir: {}".format(e))
        self.assertNotEqual(FILEPATH, result)

    def test_is_file(self):
        result = None
        try:
            result = settings.file_path(FILEPATH)
        except FileNotFoundError as e:
            print("[ERROR] test_is_file: {}".format(e))
        self.assertEqual(FILEPATH, result)

    def test_is_not_file(self):
        result = None
        try:
            result = settings.file_path(APP_ROOT)
        except FileNotFoundError as e:
            print("[ERROR] test_is_not_file: {}".format(e))
        self.assertNotEqual(APP_ROOT, result)

    def test_sanitize_path(self):
        path = "c:\\somepath/folder/../../filename.txt"
        sanitized_path = settings.sanitize_path(path)
        self.assertEqual(sanitized_path, "c:/somepath/folder/filename.txt")

class Test_Initialize(unittest.TestCase):
    """Test the settings functions in the settings module."""
    #pylint: disable=missing-function-docstring

    def test_initialize(self):
        # Set the argv values
        sys.argv = []
        sys.argv.append(FILEPATH)

        settings.Initialize()

        self.assertEqual(settings.APP_ROOT, APP_ROOT)
        self.assertEqual(settings.SETTINGS.fileName(), SETTINGSPATH)
        self.assertEqual(settings.MODULE_PATH, MODULEPATH)

    def test_initialize_ini(self):
        # Set the argv values
        sys.argv = []
        sys.argv.append(FILEPATH)
        sys.argv.append("-config={}".format(SETTINGSPATH2))

        settings.Initialize()

        self.assertEqual(settings.APP_ROOT, APP_ROOT)
        self.assertEqual(settings.SETTINGS.fileName(), SETTINGSPATH2)
        self.assertEqual(settings.MODULE_PATH, MODULEPATH)

    def test_initialize_module(self):
        # Set the argv values
        sys.argv = []
        sys.argv.append(FILEPATH)
        sys.argv.append("-module_path={}".format(MODULEPATH2))

        settings.Initialize()

        self.assertEqual(settings.APP_ROOT, APP_ROOT)
        self.assertEqual(settings.SETTINGS.fileName(), SETTINGSPATH)
        self.assertEqual(settings.MODULE_PATH, MODULEPATH2)

    def test_initialize_modules(self):
        # Set the argv values
        sys.argv = []
        sys.argv.append(FILEPATH)
        sys.argv.append("-module_path={}".format(MODULEPATH))
        sys.argv.append("-add_module_path={}".format(MODULEPATH2))

        settings.Initialize()

        self.assertEqual(MODULEPATH in settings.MODULE_PATHS, True)
        self.assertEqual(MODULEPATH2 in settings.MODULE_PATHS, True)

class Test_Settings_Load(unittest.TestCase):
    """Test the settings functions in the settings module."""
    #pylint: disable=missing-function-docstring

    def setUp(self):
        settings.SETTINGS = settings.LoadSettings(overridesettingsfile=SETTINGSPATH2)
        self.toolmanagerwidget = ToolManager(objectName="dockwindow00", load_widgets=False)
        self.tooldockwidget = ToolDockWidget(objectName="testtooldockwidget")

    # --------------------------------------------------------------------------
    #region LoadSettings
    #
    def test_loadsettings(self):
        s = settings.LoadSettings()
        self.assertEqual(type(s), QtCore.QSettings)
        self.assertEqual(s.fileName(), SETTINGSPATH)

    def test_loadsettings_override(self):
        s = settings.LoadSettings(overridesettingsfile=SETTINGSPATH2)
        self.assertEqual(type(s), QtCore.QSettings)
        self.assertEqual(s.fileName(), SETTINGSPATH2)
    #endregion LoadSettings

    # --------------------------------------------------------------------------
    #region GetSettings All
    #
    def test_getsettings_all_string(self):
        contents = settings.GetSettings("dockwindow00")
        # for key in contents:
        #     print("{}: {}".format(key, repr(contents[key])))

        self.assertEqual(repr(contents["geometry"]), r"PySide2.QtCore.QByteArray(b'\x01\xd9\xd0\xcb\x00\x02\x00\x00\x00\x00\x04\xf6\x00\x00\x00\xc0\x00\x00\x08\xb0\x00\x00\x04\x10\x00\x00\x04\xfe\x00\x00\x00\xdf\x00\x00\x08\xa8\x00\x00\x04\x08\x00\x00\x00\x00\x00\x00\x00\x00\n\x00')")
        self.assertEqual(repr(contents["windowState"]), r"PySide2.QtCore.QByteArray(b'\x00\x00\x00\xff\x00\x00\x00\x00\xfd\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x03\xab\x00\x00\x03\x16\xfc\x02\x00\x00\x00\x01\xfc\x00\x00\x00\x14\x00\x00\x03\x16\x00\x00\x01L\x00\xff\xff\xff\xfc\x01\x00\x00\x00\x02\xfc\x00\x00\x00\x00\x00\x00\x01f\x00\x00\x01f\x00\xff\xff\xff\xfc\x02\x00\x00\x00\x02\xfb\x00\x00\x00\x12\x00D\x00e\x00s\x00p\x00a\x00r\x00k\x00l\x00e\x01\x00\x00\x00\x14\x00\x00\x00\xae\x00\x00\x00\x91\x00\xff\xff\xff\xfc\x00\x00\x00\xc6\x00\x00\x02d\x00\x00\x00\xb7\x00\xff\xff\xff\xfa\x00\x00\x00\x00\x01\x00\x00\x00\x02\xfb\x00\x00\x00\x18\x00N\x00e\x00t\x00w\x00o\x00r\x00k\x00 \x00T\x00e\x00s\x00t\x01\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x01Q\x00\xff\xff\xff\xfb\x00\x00\x00$\x00T\x00e\x00s\x00t\x00 \x00I\x00n\x00p\x00u\x00t\x00 \x00W\x00i\x00d\x00g\x00e\x00t\x00s\x01\x00\x00\x00\x00\x00\x00\x01\x97\x00\x00\x00\xa8\x00\xff\xff\xff\xfb\x00\x00\x00\x1c\x00M\x00a\x00n\x00a\x00g\x00e\x00 \x00M\x00o\x00d\x00u\x00l\x00e\x00s\x01\x00\x00\x01j\x00\x00\x02A\x00\x00\x00\xba\x00\xff\xff\xff\x00\x00\x00\x00\x00\x00\x03\x16\x00\x00\x00\x04\x00\x00\x00\x04\x00\x00\x00\x08\x00\x00\x00\x08\xfc\x00\x00\x00\x00')")
        self.assertEqual(contents["windows/size"], "0")
        self.assertEqual(contents["modules/size"], "4")
        self.assertEqual(contents["modules/1/modules"], "General/manage_modules")
        self.assertEqual(contents["modules/2/modules"], "Geometry/Desparkle")
        self.assertEqual(contents["modules/3/modules"], "Network/Network_Test")
        self.assertEqual(contents["modules/4/modules"], "General/test_input_widgets")

    def test_getsettings_all_qmainwindow(self):
        contents = settings.GetSettings(self.toolmanagerwidget)

        self.assertEqual(repr(contents["geometry"]), r"PySide2.QtCore.QByteArray(b'\x01\xd9\xd0\xcb\x00\x02\x00\x00\x00\x00\x04\xf6\x00\x00\x00\xc0\x00\x00\x08\xb0\x00\x00\x04\x10\x00\x00\x04\xfe\x00\x00\x00\xdf\x00\x00\x08\xa8\x00\x00\x04\x08\x00\x00\x00\x00\x00\x00\x00\x00\n\x00')")
        self.assertEqual(repr(contents["windowState"]), r"PySide2.QtCore.QByteArray(b'\x00\x00\x00\xff\x00\x00\x00\x00\xfd\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x03\xab\x00\x00\x03\x16\xfc\x02\x00\x00\x00\x01\xfc\x00\x00\x00\x14\x00\x00\x03\x16\x00\x00\x01L\x00\xff\xff\xff\xfc\x01\x00\x00\x00\x02\xfc\x00\x00\x00\x00\x00\x00\x01f\x00\x00\x01f\x00\xff\xff\xff\xfc\x02\x00\x00\x00\x02\xfb\x00\x00\x00\x12\x00D\x00e\x00s\x00p\x00a\x00r\x00k\x00l\x00e\x01\x00\x00\x00\x14\x00\x00\x00\xae\x00\x00\x00\x91\x00\xff\xff\xff\xfc\x00\x00\x00\xc6\x00\x00\x02d\x00\x00\x00\xb7\x00\xff\xff\xff\xfa\x00\x00\x00\x00\x01\x00\x00\x00\x02\xfb\x00\x00\x00\x18\x00N\x00e\x00t\x00w\x00o\x00r\x00k\x00 \x00T\x00e\x00s\x00t\x01\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x01Q\x00\xff\xff\xff\xfb\x00\x00\x00$\x00T\x00e\x00s\x00t\x00 \x00I\x00n\x00p\x00u\x00t\x00 \x00W\x00i\x00d\x00g\x00e\x00t\x00s\x01\x00\x00\x00\x00\x00\x00\x01\x97\x00\x00\x00\xa8\x00\xff\xff\xff\xfb\x00\x00\x00\x1c\x00M\x00a\x00n\x00a\x00g\x00e\x00 \x00M\x00o\x00d\x00u\x00l\x00e\x00s\x01\x00\x00\x01j\x00\x00\x02A\x00\x00\x00\xba\x00\xff\xff\xff\x00\x00\x00\x00\x00\x00\x03\x16\x00\x00\x00\x04\x00\x00\x00\x04\x00\x00\x00\x08\x00\x00\x00\x08\xfc\x00\x00\x00\x00')")
        self.assertEqual(contents["windows/size"], "0")
        self.assertEqual(contents["modules/size"], "4")
        self.assertEqual(contents["modules/1/modules"], "General/manage_modules")
        self.assertEqual(contents["modules/2/modules"], "Geometry/Desparkle")
        self.assertEqual(contents["modules/3/modules"], "Network/Network_Test")
        self.assertEqual(contents["modules/4/modules"], "General/test_input_widgets")

    def test_getsettings_all_qdockwidget(self):
        contents = settings.GetSettings(self.tooldockwidget)
        self.assertEqual(contents["edit_ip"], "127.0.0.1")
    #endregion GetSettings All

    # --------------------------------------------------------------------------
    #region GetSettings Key
    #
    def test_getsettings_key_string(self):
        contents = settings.GetSettings("dockwindow00", "geometry")
        self.assertEqual(repr(contents["geometry"]), r"PySide2.QtCore.QByteArray(b'\x01\xd9\xd0\xcb\x00\x02\x00\x00\x00\x00\x04\xf6\x00\x00\x00\xc0\x00\x00\x08\xb0\x00\x00\x04\x10\x00\x00\x04\xfe\x00\x00\x00\xdf\x00\x00\x08\xa8\x00\x00\x04\x08\x00\x00\x00\x00\x00\x00\x00\x00\n\x00')")

    def test_getsettings_key_qmainwindow(self):
        contents = settings.GetSettings(self.toolmanagerwidget, "geometry")
        self.assertEqual(repr(contents["geometry"]), r"PySide2.QtCore.QByteArray(b'\x01\xd9\xd0\xcb\x00\x02\x00\x00\x00\x00\x04\xf6\x00\x00\x00\xc0\x00\x00\x08\xb0\x00\x00\x04\x10\x00\x00\x04\xfe\x00\x00\x00\xdf\x00\x00\x08\xa8\x00\x00\x04\x08\x00\x00\x00\x00\x00\x00\x00\x00\n\x00')")

    def test_getsettings_key_qdockwidget(self):
        contents = settings.GetSettings(self.tooldockwidget, "edit_ip")
        self.assertEqual(contents["edit_ip"], "127.0.0.1")
    #endregion Key

    # --------------------------------------------------------------------------
    #region GetSettings KeyList
    #
    def test_getsettings_keylist_string(self):
        contents = settings.GetSettings("dockwindow00", ["geometry", "windowState"])
        self.assertEqual(repr(contents["geometry"]), r"PySide2.QtCore.QByteArray(b'\x01\xd9\xd0\xcb\x00\x02\x00\x00\x00\x00\x04\xf6\x00\x00\x00\xc0\x00\x00\x08\xb0\x00\x00\x04\x10\x00\x00\x04\xfe\x00\x00\x00\xdf\x00\x00\x08\xa8\x00\x00\x04\x08\x00\x00\x00\x00\x00\x00\x00\x00\n\x00')")
        self.assertEqual(repr(contents["windowState"]), r"PySide2.QtCore.QByteArray(b'\x00\x00\x00\xff\x00\x00\x00\x00\xfd\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x03\xab\x00\x00\x03\x16\xfc\x02\x00\x00\x00\x01\xfc\x00\x00\x00\x14\x00\x00\x03\x16\x00\x00\x01L\x00\xff\xff\xff\xfc\x01\x00\x00\x00\x02\xfc\x00\x00\x00\x00\x00\x00\x01f\x00\x00\x01f\x00\xff\xff\xff\xfc\x02\x00\x00\x00\x02\xfb\x00\x00\x00\x12\x00D\x00e\x00s\x00p\x00a\x00r\x00k\x00l\x00e\x01\x00\x00\x00\x14\x00\x00\x00\xae\x00\x00\x00\x91\x00\xff\xff\xff\xfc\x00\x00\x00\xc6\x00\x00\x02d\x00\x00\x00\xb7\x00\xff\xff\xff\xfa\x00\x00\x00\x00\x01\x00\x00\x00\x02\xfb\x00\x00\x00\x18\x00N\x00e\x00t\x00w\x00o\x00r\x00k\x00 \x00T\x00e\x00s\x00t\x01\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x01Q\x00\xff\xff\xff\xfb\x00\x00\x00$\x00T\x00e\x00s\x00t\x00 \x00I\x00n\x00p\x00u\x00t\x00 \x00W\x00i\x00d\x00g\x00e\x00t\x00s\x01\x00\x00\x00\x00\x00\x00\x01\x97\x00\x00\x00\xa8\x00\xff\xff\xff\xfb\x00\x00\x00\x1c\x00M\x00a\x00n\x00a\x00g\x00e\x00 \x00M\x00o\x00d\x00u\x00l\x00e\x00s\x01\x00\x00\x01j\x00\x00\x02A\x00\x00\x00\xba\x00\xff\xff\xff\x00\x00\x00\x00\x00\x00\x03\x16\x00\x00\x00\x04\x00\x00\x00\x04\x00\x00\x00\x08\x00\x00\x00\x08\xfc\x00\x00\x00\x00')")

    def test_getsettings_keylist_qmainwindow(self):
        contents = settings.GetSettings(self.toolmanagerwidget, ["geometry", "windowState"])
        self.assertEqual(repr(contents["geometry"]), r"PySide2.QtCore.QByteArray(b'\x01\xd9\xd0\xcb\x00\x02\x00\x00\x00\x00\x04\xf6\x00\x00\x00\xc0\x00\x00\x08\xb0\x00\x00\x04\x10\x00\x00\x04\xfe\x00\x00\x00\xdf\x00\x00\x08\xa8\x00\x00\x04\x08\x00\x00\x00\x00\x00\x00\x00\x00\n\x00')")
        self.assertEqual(repr(contents["windowState"]), r"PySide2.QtCore.QByteArray(b'\x00\x00\x00\xff\x00\x00\x00\x00\xfd\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x03\xab\x00\x00\x03\x16\xfc\x02\x00\x00\x00\x01\xfc\x00\x00\x00\x14\x00\x00\x03\x16\x00\x00\x01L\x00\xff\xff\xff\xfc\x01\x00\x00\x00\x02\xfc\x00\x00\x00\x00\x00\x00\x01f\x00\x00\x01f\x00\xff\xff\xff\xfc\x02\x00\x00\x00\x02\xfb\x00\x00\x00\x12\x00D\x00e\x00s\x00p\x00a\x00r\x00k\x00l\x00e\x01\x00\x00\x00\x14\x00\x00\x00\xae\x00\x00\x00\x91\x00\xff\xff\xff\xfc\x00\x00\x00\xc6\x00\x00\x02d\x00\x00\x00\xb7\x00\xff\xff\xff\xfa\x00\x00\x00\x00\x01\x00\x00\x00\x02\xfb\x00\x00\x00\x18\x00N\x00e\x00t\x00w\x00o\x00r\x00k\x00 \x00T\x00e\x00s\x00t\x01\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x01Q\x00\xff\xff\xff\xfb\x00\x00\x00$\x00T\x00e\x00s\x00t\x00 \x00I\x00n\x00p\x00u\x00t\x00 \x00W\x00i\x00d\x00g\x00e\x00t\x00s\x01\x00\x00\x00\x00\x00\x00\x01\x97\x00\x00\x00\xa8\x00\xff\xff\xff\xfb\x00\x00\x00\x1c\x00M\x00a\x00n\x00a\x00g\x00e\x00 \x00M\x00o\x00d\x00u\x00l\x00e\x00s\x01\x00\x00\x01j\x00\x00\x02A\x00\x00\x00\xba\x00\xff\xff\xff\x00\x00\x00\x00\x00\x00\x03\x16\x00\x00\x00\x04\x00\x00\x00\x04\x00\x00\x00\x08\x00\x00\x00\x08\xfc\x00\x00\x00\x00')")

    def test_getsettings_keylist_qdockwidget(self):
        contents = settings.GetSettings(self.tooldockwidget, ["edit_ip", "edit_ip2"])
        self.assertEqual(contents["edit_ip"], "127.0.0.1")
        self.assertEqual(contents["edit_ip2"], "10.0.0.1")
    #endregion KeyList

    # --------------------------------------------------------------------------
    #region GetSettings Array
    #
    def test_getsettings_array_string(self):
        contents = settings.GetSettingsArray("dockwindow00", "modules")

        self.assertEqual(contents[0], "General/manage_modules")
        self.assertEqual(contents[1], "Geometry/Desparkle")
        self.assertEqual(contents[2], "Network/Network_Test")
        self.assertEqual(contents[3], "General/test_input_widgets")

    def test_getsettings_array_qmainwindow(self):
        contents = settings.GetSettingsArray(self.toolmanagerwidget, "modules")

        self.assertEqual(contents[0], "General/manage_modules")
        self.assertEqual(contents[1], "Geometry/Desparkle")
        self.assertEqual(contents[2], "Network/Network_Test")
        self.assertEqual(contents[3], "General/test_input_widgets")

    def test_getsettings_array_qdockwidget(self):
        contents = settings.GetSettingsArray(self.tooldockwidget, "modules")

        self.assertEqual(contents[0], "General/manage_modules")
        self.assertEqual(contents[1], "Geometry/Desparkle")
        self.assertEqual(contents[2], "Network/Network_Test")
        self.assertEqual(contents[3], "General/test_input_widgets")

    def test_getsettings_array_string_empty(self):
        contents = settings.GetSettingsArray("dockwindow00", "windows")
        self.assertEqual(len(contents), 0)

    def test_getsettings_array_qmainwindow_empty(self):
        contents = settings.GetSettingsArray(self.toolmanagerwidget, "windows")
        self.assertEqual(len(contents), 0)

    def test_getsettings_array_qdockwidget_empty(self):
        contents = settings.GetSettingsArray(self.tooldockwidget, "windows")
        self.assertEqual(len(contents), 0)
    #endregion Array

class Test_Settings_Save(unittest.TestCase):
    """Test the settings functions in the settings module."""
    #pylint: disable=missing-function-docstring

    def setUp(self):
        settings.SETTINGS = settings.LoadSettings(overridesettingsfile=SETTINGSPATH2)
        self.toolmanagerwidget = ToolManager(objectName="dockwindow01", load_widgets=False)
        self.tooldockwidget = ToolDockWidget(objectName="testtooldockwidget")

    def generate_random_string(self):
        """Generate a random alphanumeric string between 10 and 20 characters long."""

        value = ""
        length = random.randrange(10, 20)

        for i in range(length):
            rnd_index = random.randrange(0, len(ALPHABET))
            value += ALPHABET[rnd_index]

        print("Test Value: {}".format(value))
        return(value)

    # --------------------------------------------------------------------------
    #region SetSettings - single key
    #
    def test_setsettings_string(self):
        value = self.generate_random_string()
        settings.SetSettings("dockwindow01", "key", value)
        content = settings.GetSettings("dockwindow01", "key")
        self.assertEqual("key" in content, True)
        self.assertEqual(content["key"], value)

    def test_setsettings_qmainwindow(self):
        value = self.generate_random_string()
        settings.SetSettings(self.toolmanagerwidget, "key", value)
        content = settings.GetSettings(self.toolmanagerwidget, "key")
        self.assertEqual("key" in content, True)
        self.assertEqual(content["key"], value)

    def test_setsettings_qdockwidget(self):
        value = self.generate_random_string()
        settings.SetSettings(self.tooldockwidget, "key", value)
        content = settings.GetSettings(self.tooldockwidget, "key")
        self.assertEqual("key" in content, True)
        self.assertEqual(content["key"], value)
    #endregion - single key

    # --------------------------------------------------------------------------
    #region SetSettings - array
    #




    #endregion - array

class Test_Settings_Modules(unittest.TestCase):
    """Test the module related functions in the settings module."""
    #pylint: disable=missing-function-docstring

    def setUp(self):
        settings.SETTINGS = settings.LoadSettings(overridesettingsfile=SETTINGSPATH2)

        # # Populate with some random module names
        # self.widget.modules = []
        # module_count = random.randrange(1, 4)
        # for i in range(module_count):
        #     module_name = ALPHABET[i]
        #     self.widget.modules.append(module_name)

        # # Populate with some random windows
        # self.widget.windows = []
        # window_count = random.randrange(1, 4)
        # for i in range(window_count):
        #     window_name = "testwindow0{}".format(i + 1)
        #     self.widget.windows.append(window_name)

    # GetModuleName
    # GetRelativeModulePath
    # GetModulePaths
    # LoadModule
    # GetModuleWidget

    def test_get_module_name(self):
        pass

    def test_get_relative_module_path(self):
        pass

    def test_get_module_paths(self):
        pass

    def test_load_module(self):
        pass

    def test_get_module_widget(self):
        pass


#endregion Tests

# --------------------------------------------------------------------------------------------------
#region Main
#
if __name__ == '__main__':
    unittest.main()
#endregion Main
