"""
Custom class to handle managing config options, saving, and loading.
"""
from __future__ import print_function
import os
import sys
import json
from datetime import datetime
import requests
import inspect
try:
    import MySQLdb
    MYSQL = True
except ImportError as e:
    MYSQL = False

from lib.Log import Log

# ------------------------------------------------------------------------------------------------------------------
#region Config
#
class Config(object):
    """
    Custom class to handle managing config options, saving, and loading.

    On creation, set the preferred file location for the config settings to be stored and read from.

    Accessing the settings is done as if the class was a dictionary:
    config = Config(path="c:/path/to/app/config.cfg")

    mysqlsettings = {
        server: "localhost",
        port: 1666,
        user: "mysqluser",
        password: "password",
        database: "configdb",
        }
    config = Config(mysql=mysqlsettings)

    config.load()

    config["some_config_key"] = "some value"
    if config["some_other_key"] == True:
        pass

    """
    # ------------------------------------------------------------------------------------------------------------------
    #region Initialize
    #
    def __init__(self, path=None, autoload=False, mysql=None):
        """
        [summary]

        Keyword Arguments:
            path {str} -- If set, specifies the target file (default: {None})
            mysql {[type]} -- [description] (default: {None})
        """
        output = ""

        self.debug = True

        # Initialize the class instance's settings
        self.path = None
        self.value = {}     # Holds the settings

        # MySQL -
        self.server = None
        self.database = None

        # MySQL - local
        self.key = None

        # Figure out if this config instance reads the settings from file or database
        if path:
            self.path = path

        if autoload:
            self.display("{}".format("Loaded" if self.load() else "Failed to load"))


        # MySQL stuff
        if MYSQL:
            if mysql and "mysql" in self.value:
                self.server = self.value["mysql"]
                self.connect()

            elif "mysql" not in self.value: # Set Defaults
                mysql = {}
                mysql["server"] = "192.168.1.20"
                mysql["port"] = 3306
                mysql["user"] = "kobibot"
                mysql["password"] = "kobibotpassword"
                mysql["database"] = "kobibot"
                self.value["mysql"] = mysql

        Log(output)
    #endregion


    # ------------------------------------------------------------------------------------------------------------------
    #region Indexer
    #
    def __repr__(self):
        return repr(self.value)

    def __getitem__(self, item):
        if isinstance(self.value[item], Config):
            pass

        return self.value[item]

    def __setitem__(self, item, value):
        if item in self.value and isinstance(self.value[item], Config):
            pass
        self.value[item] = value

    def __delitem__(self, item):
        del self.value[item]
    #endregion


    # ----------------------------------------------------------------------------------------------------------------------
    #region Utilities
    #
    def walk_dict(self, source_dict, depth=1, padding="\t"):
        """
        Walks the dictionary and outputs the contents.

        Arguments:
            d {dict} -- The Dictionary to walk and output.

        Keyword Arguments:
            depth {int} -- How much padding for each level
            padding {string} -- the padding to prefix each line. Defaults to "\t"

        Returns:
            string -- A human-readable output of the dictionary contents.
        """
        output = ""

        for key, value in sorted(source_dict.items(), key=lambda x: x[0]):
            if isinstance(value, dict):
                output += padding*depth + "{}\n".format(key)
                output += padding*depth + "{\n"
                output += self.walk_dict(value, depth + 1)
                output += padding*depth + "}\n"
            else:
                output += padding*depth + "{}: {}\n".format(key, value)
        return output

    def display(self, description=None):
        """
        Displays the contents of the config's settings.

        Keyword Arguments:
            description {[type]} -- [description] (default: {None})
        """
        output = ""
        output += self.walk_dict(self.value)
        Log(output, description)

    def get_timestamp(self, time_format="%Y-%m-%d %H:%M:%S"):
        """
        Returns the current timestamp in the specified format, defaults to YYYY-MM-DD HH:MM:SS

        Returns:
            [type] -- [description]
        """

        return datetime.now().strftime(time_format)

    #endregion


    # ------------------------------------------------------------------------------------------------------------------
    #region I/O
    #
    def save(self):
        """
        Saves the current config data to the stored config file.
        """
        try:
            with open(self.path, "w") as write_file:
                json.dump(self.value, write_file, skipkeys=True, indent=4, sort_keys=True) # indent="\t"
            return True
        except IOError as exception:
            Log("ERROR: {}".format(exception))
            return False

    def load(self):
        """
        Loads the config from the stored config file.

        Returns:
            boolean -- True if successfully read and parsed the JSON config file, otherwise False (No file found, incorrect JSON format)
        """
        try:
            with open(self.path, "r") as read_file:
                self.value = json.load(read_file)
            return True
        except ValueError as exception:
            Log("ERROR: {}".format(exception))
        except IOError as exception:
            Log("ERROR: {}".format(exception))
        except Exception as exception: # pylint: disable=broad-except
            Log("ERROR: {}".format(exception))

        return False

    def load_from_internet(self):
        """
        Test JSON - https://realpython.com/python-json/
        """
        response = requests.get("https://jsonplaceholder.typicode.com/todos")
        self.value = json.loads(response.text)
    #endregion


    # ------------------------------------------------------------------------------------------------------------------
    #region MySQL
    #
    def connect(self):
        """
        Connect to the specified MySQL server and database.
        """
        output = ""
        if not MYSQL:
            print("MySQL not available")
            return

        if "mysql" in self.value:
            mysql = self.value["mysql"] # load the MySQL config block

            # Open database connection
            try:
                self.database = MySQLdb.connect(mysql["server"], mysql["user"], mysql["password"], mysql["database"])
                output += "- Opening MySQL: {}\n".format(self.database)
            except Exception as e:
                output += "Unable to connect to MySQL: {}".format(e)

        else:
            output += "MySQL not enabled."

            # disconnect from server
            # self.database.close()
        Log(output)

    def disconnect(self):
        """
        Disconnect from any active MySQL connections.
        """
        if self.database:
            self.database.close()

    def load_from_mysql(self, key):
        """
        [summary]

        Arguments:
            key {[type]} -- [description]

        Returns:
            [type] -- [description]
        """
        output = ""
        result = False
        if self.database:
            db_cursor = self.database.cursor()
            sql = "SELECT k, v FROM `{}` WHERE k='{}'".format("configtable", key)
            output += "- SQL: {}\n".format(sql)
            db_cursor.execute(sql)
            if db_cursor.rowcount == 0: # Not Found
                output += "No items found."
            else:
                for row in db_cursor.fetchall():
                    self.value[key] = json.loads(row[1])
                    result = True

        Log(output)
        return result

    def save_to_mysql(self, key):
        """
        [summary]

        Arguments:
            key {[type]} -- [description]
        """
        output = ""
        if self.database:
            json_value = json.dumps(self.value[key], sort_keys=True) # indent="\t"

            db_cursor = self.database.cursor()

            # check if this key exists already
            query = "SELECT %s FROM `configtable` WHERE k=%s;"
            val = ("k", key)
            db_cursor.execute(query, val)

            if db_cursor.rowcount > 0: # Update
                query = "UPDATE `configtable` SET v=%s, last_modified=%s, last_modified_timestamp=%s WHERE k=%s;"
                val = (json_value, self.get_timestamp(), datetime.now().timestamp(), key)
                output += "- SQL query: {}\n".format(query % val)
            else: # Insert
                query = "INSERT INTO `configtable` VALUES(%s, %s, %s, %s);"
                val = (key, json_value, self.get_timestamp(), datetime.now().timestamp())
                output += "- SQL query: {}\n".format(query % val)

            db_cursor.execute(query, val)
            self.database.commit()

        Log(output)

    #endregion


#endregion
