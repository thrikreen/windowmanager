# ----------------------------------------------------------------------------------------------------------------------
#region Modules
#
import os
import sys
from datetime import datetime
import importlib

try:
    import Qt
    from Qt import QtGui, QtWidgets, QtCore
    from Qt import QUiLoader
    # print("[WM] import Qt")
except ImportError as e:
    try:
        import PySide2 # QT5, Max 2017+, Maya
        from PySide2 import QtGui, QtWidgets, QtCore
        from PySide2.QtUiTools import QUiLoader
        # print("[WM] import PySide2")
    except ImportError as e:
        try:
            import PySide # QT4, Max 2015
            from PySide import QtGui, QtCore
            from PySide import QtGui as QtWidgets
            from PySide import QUiLoader
            # print("[WM] import PySide")
        except ImportError as e: # Exit since no Qt libraries are available
            # print("[WM] ERROR: {}".format(e))
            exit()

from lib.ToolDockWidget import ToolDockWidget
#endregion Modules

# Module Info
name = "Search"
description = "Search tool modules to add."
version = 1.0
tags = [
    "search",
    "modules",
]

# ----------------------------------------------------------------------------------------------------------------------
#region QDockWidget Class
#
class Search(ToolDockWidget):
    """
    Search [summary]

    Arguments:
        ToolDockWidget {[type]} -- [description]

    Returns:
        [type] -- [description]
    """

    # ----------------------------------------------------------------------------------------------------------------------
    #region Init
    #
    def __init__(self, parent=None, objectName=None):
        """
        __init__ [summary]

        Keyword Arguments:
            parent {[type]} -- [description] (default: {None})
        """
        self.help_url = "http://intranet/dept/arttools/{}".format(self.__class__.__name__)
        super(Search, self).__init__(parent)
        output_text = []
        output_text.append("search.py")

        # Load UI
        self.setObjectName(objectName)
        self.setWindowTitle(objectName)
        self.load_ui(__file__)

        # Connect events
        if self.widget:
            self.widget.button_search.clicked.connect(self.on_search)
            self.widget.button_clear.clicked.connect(self.on_clear)
            self.widget.lineedit_search.textChanged.connect(self.on_text_changed)
            self.widget.lineedit_search.returnPressed.connect(self.on_search)

            self.widget.tree_modules.itemClicked.connect(self.on_item_clicked)
            self.widget.tree_modules.itemDoubleClicked.connect(self.on_item_doubleclicked)

        # Restore any stored settings
        self.restore_settings()

        self.log(output_text)
    #endregion

    # ----------------------------------------------------------------------------------------------------------------------
    #region UI Events
    #
    def on_search(self):
        output_text = []
        text = self.widget.lineedit_search.text()
        output_text.append(text)
        self.log(output_text)
        self.search_modules(text)

    def on_clear(self):
        output_text = []
        self.widget.lineedit_search.setText("")
        self.log(output_text)

    def on_text_changed(self):
        # output_text = []
        # text = self.widget.lineedit_search.text()
        # output_text.append(text)
        # self.log(output_text)
        pass

    def on_item_clicked(self):
        output_text = []
        self.log(output_text)

    def on_item_doubleclicked(self):
        output_text = []
        output_text.append("Parent: {}".format(self.parent()))
        output_text.append("Modules: {}".format(self.parent().modules))
        self.log(output_text)

    #endregion UI Events

    # ----------------------------------------------------------------------------------------------------------------------
    #region Functions
    #
    def search_modules(self, search_terms):
        output_text = []

        self.widget.tree_modules.clear()

        search_tags = search_terms.lower().split(" ")

        # p = r"F:\projects\square\windowmanager\arttools.py"
        module_root = r"C:/Workspaces/WindowManager/"
        root_path = os.path.dirname(module_root)
        modules_pathname = "modules"

        root_modules_path = os.path.join(root_path, modules_pathname)

        output_text.append("- search_terms to tags: {} -> {}".format(search_terms, search_tags))
        output_text.append("- root_path: {}".format(root_path))
        output_text.append("- modules_pathname: {}".format(modules_pathname))
        output_text.append("- root_modules_path: {}".format(root_modules_path))

        for root, dirs, files in os.walk(root_modules_path):
            dirs[:] = [d for d in dirs if not d[0] in ['.', '_']] # filter out hidden or system folders
            for d in dirs: # Create a tab for each folder
                subpath = os.path.join(root, d)
                output_text.append("\t- Sub Path: {}".format(subpath))

                for subroot, subdirs, subfiles in os.walk(subpath):
                    subfiles = [f for f in subfiles if not (f[0] in ['.', '_'] or not os.path.splitext(f)[1].lower() == ".py")] # collect files if not hidden or not a .py file
                    for f in subfiles:
                        module_filepath = os.path.join(subroot, f) # get the abs path of the module
                        module_filepath = module_filepath.replace(root_path, "").replace("/", ".").replace("\\", ".") # strip prefix, replace path slashes with .

                        module_path = os.path.splitext(module_filepath)[0] # Remove the trailing extension
                        if module_path[0] == ".": # remove the leading .
                            module_path = module_path[1:]
                        output_text.append("\t\t- {}".format(module_path))

                        # The Module and see if the search terms match any of the tags
                        module = importlib.import_module(module_path)
                        if hasattr(module, "tags"):
                            output_text.append("\t\t- Module Tags: {}".format(module.tags))

                            display_module = False
                            for tag in search_tags:
                                if tag in module.tags:
                                    display_module = True
                                    break

                            if display_module:

                                module_item = QtWidgets.QTreeWidgetItem(self.widget.tree_modules)
                                module_item.setText(0, "{}".format(module.name))
                                module_item.setText(1, "{}".format(module.description))
                                module_item.setText(2, "{}".format(module.version))

                                module_item.setToolTip(1, "{}".format(module.description))

                                self.widget.tree_modules.addTopLevelItem(module_item)

        self.log(output_text)
    #endregion Functions



#endregion QDockWidget Class

# ----------------------------------------------------------------------------------------------------------------------
#region Core Widget Functions
#
def get_widget(parent=None):
    widget = Search(parent=parent, objectName="Search")
    return widget
#endregion Core Widget Functions
